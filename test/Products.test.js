const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const { expect } = chai;

before(done => {
    console.log('\n\n-----------------------\n--\n-- START TEST\n--\n-------------------------');
    done();
});
after(done => {
    console.log('\n\n-----------------------\n--\n-- END TEST\n--\n-------------------------');
    done();
});
/* asyn test */
describe('#Test api products ripley', () => {
  it('get "products" record', done => {
    chai.request('http://localhost:3000')
      .get('/products')
      .end(function (err, res) {
        if(err) done(err);
        expect(res).to.have.status(200);
        expect(res.body.length).to.equals(4);
        done();
        console.log('status code: %s, products: %s',res.statusCode, res.body.length)
      });
  }).timeout(0);

  it('get "products route not exist"', done => {
    chai.request('http://localhost:3000')
      .get('/product')
      .end(function (err, res) {
        if(err) done(err);
        expect(res).to.have.status(404);
        done();
      });
  }).timeout(0);

  it('get "product by id" record', done => {
    chai.request('http://localhost:3000')
      .get('/products/12747628')
      .end(function (err, res) {
        if(err) done(err);
        expect(res).to.have.status(200);
        expect(res.body.uniqueID).to.equals('12747628');
        done();
      });
  }).timeout(0);

  it('get "product by id not exist" record', done => {
    chai.request('http://localhost:3000')
      .get('/products/12')
      .end(function (err, res) {
        if(err) done(err);
        expect(res.body.message).to.equals('Request failed with status code 404');
        done();
        console.log('status code: %s, user: %s',res.statusCode, res.body, false)
      });
  }).timeout(0);

});
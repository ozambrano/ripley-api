const express = require('express');
const app = express();
const productRoutes = require('./src/routes/ProductsRoutes');
const authMiddleware = require('./middleware/firebase-middleware');

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/products', authMiddleware.checkIfAuthenticated, productRoutes);
 
app.listen(8080, function () {
  console.log('Example app listening on port 3000!');
});
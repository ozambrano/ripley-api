const client = require('../../../config/redis');
const productsService = require('../../services/products/ProductsService');
// Display list of all Products.
exports.products_list = async (req, res) => {
  const partNumbers = ['275668', '275669', '275679', '275649'];
  const products = await productsService.products_list(partNumbers);

  return res.json(products.data);
};

// Display detail page for a specific Porducts.
exports.products_detail = async (req, res) => {

  client.get(`products:${req.params.id}`, async (err, result) => {
    if (result) {
      return res.json({source: 'cache', ...JSON.parse(result)}).status(200);
    }

    try {
      product = await productsService.products_detail(req.params.id);
      await client.setex(`products:${product.data.uniqueID}`, 120, JSON.stringify(product.data));
      return res.send({source: 'api', ...product.data}).status(200);
    }
    catch(error) {
      console.log('LOG:', error);

      const status = error.response.status;
      if(status === 404) {
        return res.json(error).status(status);
      }

      const product = await productsService.products_detail(req.params.id);
      client.setex(`products:${product.data.uniqueID}`, 120, JSON.stringify(product.data));
      return res.json({source: 'api', ...product.data});

    }

  })
};

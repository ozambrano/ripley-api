const axios = require("axios");

// Display list of all Products.
exports.products_list = async (partNumbers) => {
    return await axios.get(`https://simple.ripley.cl/api/v2/products?partNumbers=${partNumbers.join()}`);
};

// Display detail page for a specific Porducts.
exports.products_detail = async (id) => {
    return await axios.get(`https://simple.ripley.cl/api/v2/products/by-id/${id}`);
};

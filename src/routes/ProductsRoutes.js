const express = require('express');
const router = express.Router();

const productController = require('../controllers/products/ProductsController');

router.get('', productController.products_list);
router.get('/:id', productController.products_detail);

module.exports = router;
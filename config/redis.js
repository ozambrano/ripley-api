const redis = require('redis');

const REDISHOST = '10.229.81.115';
const REDISPORT = 6379;

const client = redis.createClient(REDISPORT, REDISHOST);

client.on('error', (err) => {
    console.log("Error " + err)
});

module.exports = client;
var admin = require('firebase-admin');

var serviceAccount = require('../config/api-ripley.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://api-ripley.firebaseio.com"
});

exports.checkIfAuthenticated = async (req, res, next) => {
  if (req.headers.authorization) {
    try {
      const authToken = req.headers.authorization;
      const userInfo = await admin
        .auth()
        .verifyIdToken(authToken);
      req.authId = userInfo.uid;
      return next();
    } catch (e) {
      return res
        .status(401)
        .send({ error: 'You are not authorized to make this request' });
    }
  }
  return next();
};